[image0]: ./report/training_examples.png "Examples of training set"
[image1]: ./report/data_set.png "Distribtuion of classes in data set"
[image2]: ./report/training.png "Training process"
[image3]: ./report/test_errors.png "Wrongly classified test images"
[image4]: ./report/test_accuracy_VS_abundance.png "Test accuracy VS abundance in training set"
[image5]: ./report/new_images.png "New images from Google street view"

## Traffic Sign Recognition

---

**Overview**
	
As a documentation for my project submission, this report discusses all the [rubric points](https://review.udacity.com/#!/rubrics/481/view) of the project.

---

## Dataset Exploration

**Dataset Summary**

The data set consists of 34799 training examples, 4410 validation examples, and 12630 test examples. The images in the data set are of size 32x32 pixels with RGB colors and depict 43 different classes of traffic signs.


**Exploratory Visualization**

For illustration of the data set 36 randomly selected pictures from the training set are plotted:
![alt text][image0]
The data set is somewhat unbalanced. To show this, plot histograms of the labels of training, validation, and test sets in the upper row of the following figure.
![alt text][image1]
The number of examples per class is not evenly distributed in either of the data sets. Some classes are about 10 times more abundant than others. This can lead to a poor performance of the classification of samples from the less abundant classes. Besides the obvious problem that there are less examples of variations of traffic signs of these classes to learn from, another problem is that their relative weight in the cost function is lower than for the more abundant classes. The consequences of this imbalance will be visible in the review of classification errors at the bottom of the notebook.
The population of classes in the validation and test sets are compared to the population of the training set in the lower row of the figure. The plots show the ratios of the histograms of validation over training set and test over training set, respectively. Some labels over overrepresented in the validation set. The ratio of the abundance in the validation set divided by the abundance in the training set reaches up to 22% for a some classes while most labels are at lower percentages. The population of classes in the test set is more proportional to the training set. To check whether this feature of the validation set has an effect on the performance of the classifier, the training and validation sets were merged and reshuffled, and a new validation set was separated off. This had no effect on the performance of the classifier in terms of validation accuracy.

---

## Design and Test a Model Architecture

**Preprocessing**

The images are preprocessed by normalizing them. From each pixel in each color channel, the value of 127.5 is subtracted and the result is divided by 127.5. This leads to a dynamic range of [-1, 1] for each feature. Another preprocessing step that was tested is Y-channel centering (as suggested in the Sermanet and LeCun paper). For this, the images were converted to YUV color space, the Y-channel was added or subtracted to, so that the mean value over the 32x32 pixels was 127 to provide a data set with even brightness. Then the images were converted back to RGB for classification. This technique did not lead to any improvement in the validation accuracy, which was already at 98.7% at the time. For this reason, Y-channel centering was not included in the submitted IPython notebook.
Data augmentation by translation was also tested. The images were shifted by either 2 or 4 pixels in each of the four directions and the freed up pixels were set to white color. This way, the training set size was artificially increased by a factor of four. Unfortunately, this technique did not lead to an improved validation accuracy and hence was from the submission excluded as well.

**Model Architecture**

With the original LeNet architecture the classifier reached a validation accuracy of up 95-96%. To improve the accuracy of the classifier, the number of Kernels in the convolutional layers was increased. As seems to be customary in the field, I use powers of two for the number of Kernels and a doubling of the number of Kernels from one convolutional layer to the next. When using 16 / 32 Kernels in the first / second conv layer a slight increase in validation accuracy could be reached. A further improvement was achieved by using 32 / 64 Kernels in the first / second conv layers. A further doubling of the Kernels did yield further improvements. Test also showed that 3x3 Kernels perormed slightly better than 5x5 Kernels. The number of Neurons in the first fully connected layer was reduced from 400 to 250. Attempts in reducing the size of this layer showed that such a reduction does not lead to a penalty in validation accuracy, while probably providing a slight decrease in the inference time (although this was not explicitly tested).
Two means of regularization are introduced to combat overfitting. Dropout with a 50% keep probability was used at the outputs of the fully connected layers. In addition, L2 regularization was introduced on the weights of all convolutional and fully connected layers. The biases were not included in the L2 regularization. The architecture of the used convolutional neural net is as following:

| Layer         		|     Description	        					| 
|:---------------------:|:---------------------------------------------:| 
| Input         		| 32x32x3 RGB image   							| 
| Convolution 3x3     	| 1x1 stride, same padding, outputs 32x32x32 	|
| RELU					|												|
| Max pooling	      	| 2x2 stride,  outputs 16x16x32 				|
| Convolution 3x3	    | 1x1 stride, same padding, outputs 16x16x64	|
| RELU					|												|
| Max pooling	      	| 2x2 stride,  outputs 8x8x64 					|
| Flattening			| Flatten from conv layer to flat layer with 4096 activations |
| Fully connected		| 250 neurons									|
| RELU					|												|
| Dropout				| 50% keep probability							|
| Fully connected		| 120 neurons									|
| RELU					|												|
| Dropout				| 50% keep probability							|
| Output layer			| 43 output logits								|
| Softmax				|         										|
| Loss function			| Cross entropy of logits vs. truth labels + alpha x L2_loss 				|
 The scaling parameter alpha for the L2 loss is a new hyper parameter of the model. The biases of all layers were initialized with 0.01 to avoid dead RELUs and the weights were initialized with sigma=0.1 Gaussians, centered at 0.

**Model Training**

As suggested in the LeCun project, the Adam optimizer was used. Although this does not seems to be very common practice with the Adam optimizer, I used an exponential decay of the learning rate. This lead to a significant improvement in the validation accuracy as can be seen from this plot of the training process:
![alt text][image2]
When the training and validation losses reach a plateau, reducing the learning rate by a factor of ten helps to improve things. Not only do the losses reduce further, also the validation accuracy improves and stabilizes at higher values. Some hyper parameters are chosen by hand, while others result from a hyper parameter scan. The scan was performed over 30 training runs of 200 epochs each. The code for the random initialization of the hyper parameters is not included in the IPython notebook, but in the file Traffic_Sign_Classifier.py (in the same Git repo) that was used for the development of this project. The following table lists the hyper parameter values finally used, as well as the parameter ranges used in the scan:

| Hyper parameter | value | scan range | logarithmic sampling | 
| --- | --- | --- | --- |
| Initial learning rate for Adam | 0.0015 | [1e-4, 1e-2] | yes |
| alpha (L2 loss amplitude) | 5.7e-5 | [1e-5, 1e-2] | yes |
| Nof epochs after which learning rate \*= 0.1 | 59 | [30, 80] | no |
| Batch size | 128 |  |  |
| Nof epochs for training | 200 | | |
| Dropout keep probability | 50% | | |
| Bias initialization | 0.01 | | |
| Weight initialization sigma | 0.1 |  |  |

**Solution Approach **

In my previous submission I had described my approach to the solution in the section about model architecture, which lead to a fail in the review of the project. To fix this, I repeat some of the points made earlier.

Q: What was the first architecture that was tried and why was it chosen?
A: The first architecture was taken from the LeNet lab, because this was suggested in the lecture video.

Q: What were some problems with the initial architecture?
A: The model yielded a validation accuracy of 95-96%, which would have been enough to meet the specification. However, I wanted to do better.

Q: How was the architecture adjusted and why was it adjusted?
A: The basic architecture of two convolutional layers, two fully connected layers, and an output layer was retained. However, the number of Kernels in the convolutional layers was significantly increased, because this leads to higher validation accuracy. Also, dropout as well as L2 regularization were introduced. to combat overfitting.

Q: Which parameters were tuned? How were they adjusted and why?
A: To improve the accuracy of the classifier, the number of Kernels in the convolutional layers was increased. As seems to be customary in the field, I use powers of two for the number of Kernels and a doubling of the number of Kernels from one convolutional layer to the next. When using 16 / 32 Kernels in the first / second conv layer a slight increase in validation accuracy could be reached. A further improvement was achieved by using 32 / 64 Kernels in the first / second conv layers. A further doubling of the Kernels did yield further improvements. The number of Neurons in the first fully connected layer was reduced from 400 to 250. Attempts in reducing the size of this layer showed that such a reduction does not lead to a penalty in validation accuracy, while probably providing a slight decrease in the inference time (although this was not explicitly tested).

Q: What are some of the important design choices and why were they chosen?
A: The number of Kernels in the convolutional layers was increased to improve validation accuracy. In addition, two means of regularization are introduced to combat overfitting and because they improved validation accuracy. Dropout with a 50% keep probability was used at the outputs of the fully connected layers. In addition, L2 regularization was introduced on the weights of all convolutional and fully connected layers. The biases were not included in the L2 regularization. 

**Result summary**

The validation accuracy reaches a stable level of 98.7% during training. This value can change by maybe +-0.2 % depending on the random initialization. After this satisfying result has been reached, the accuracy on the test set is evaluated to be 97.9%. This value seems reasonable as it is slightly lower than the validation accuracy. All the wrongly classified images are plotted here:
![alt text][image3]
Some of these wrongly classified test images suffer from over- or underexposure, partial shading, or other effects. Other, however, have nothing apparent visual flaws. The performance of the classifier on the individual classes is listed in detail in the following table of the test set:


| class index | class text | precision | recall |
| --- | --- | --- | --- |
| 0 | Speed limit (20km/h) | 1.000 | 0.967 |
| 1 | Speed limit (30km/h) | 0.965 | 0.993 |
| 2 | Speed limit (50km/h) | 0.989 | 0.993 |
| 3 | Speed limit (60km/h) | 0.984 | 0.971 |
| 4 | Speed limit (70km/h) | 0.995 | 0.991 |
| 5 | Speed limit (80km/h) | 0.957 | 0.979 |
| 6 | End of speed limit (80km/h) | 1.000 | 0.887 |
| 7 | Speed limit (100km/h) | 0.995 | 0.960 |
| 8 | Speed limit (120km/h) | 0.978 | 0.978 |
| 9 | No passing | 0.986 | 0.994 |
| 10 | No passing for vehicles over 3.5 metric tons | 0.989 | 0.995 |
| 11 | Right-of-way at the next intersection | 0.972 | 0.990 |
| 12 | Priority road | 0.976 | 0.981 |
| 13 | Yield | 0.993 | 0.997 |
| 14 | Stop | 1.000 | 1.000 |
| 15 | No vehicles | 0.942 | 1.000 |
| 16 | Vehicles over 3.5 metric tons prohibited | 0.987 | 0.993 |
| 17 | No entry | 1.000 | 0.997 |
| 18 | General caution | 0.995 | 0.951 |
| 19 | Dangerous curve to the left | 0.938 | 1.000 |
| 20 | Dangerous curve to the right | 0.804 | 1.000 |
| 21 | Double curve | 0.837 | 0.800 |
| 22 | Bumpy road | 0.983 | 0.983 |
| 23 | Slippery road | 0.993 | 0.980 |
| 24 | Road narrows on the right | 0.967 | 0.967 |
| 25 | Road work | 0.977 | 0.963 |
| 26 | Traffic signals | 0.964 | 0.889 |
| 27 | Pedestrians | 0.909 | 0.667 |
| 28 | Children crossing | 1.000 | 0.993 |
| 29 | Bicycles crossing | 0.978 | 1.000 |
| 30 | Beware of ice/snow | 0.977 | 0.847 |
| 31 | Wild animals crossing | 0.985 | 0.996 |
| 32 | End of all speed and passing limits | 0.882 | 1.000 |
| 33 | Turn right ahead | 0.995 | 1.000 |
| 34 | Turn left ahead | 0.992 | 0.992 |
| 35 | Ahead only | 1.000 | 0.995 |
| 36 | Go straight or right | 0.984 | 1.000 |
| 37 | Go straight or left | 0.968 | 1.000 |
| 38 | Keep right | 0.979 | 0.997 |
| 39 | Keep left | 0.989 | 0.989 |
| 40 | Roundabout mandatory | 0.989 | 0.978 |
| 41 | End of no passing | 0.821 | 0.917 |
| 42 | End of no passing by vehicles over 3.5 metric tons | 1.000 | 0.878 |


There are some classes with low recall values. As can be seen in the following figure, low recall values occur for classes with poor statisitcs in the training set:
![alt text][image4]
While there are classes with a low number of training examples and good test recall, all classes with a poor performance are low statistics classes. This problem would best be solved by collecting more data on these classes. Another possible way to improve this situation would be by duplicating the training examples of these classes (oversampling) to increase their weight in the optimization of the overall cost function.

---

## Test the Model on New Images

**Acquiring New Images**

New images of traffic signs (and some images without traffic signs) were acquired from screenshots of Google street views of Munich, Germany. These are the new images, scaled to 32x32 pixels:
![alt text][image5]
The images from street view were all of very good quality. To make things more challenging for the classifier, I increased the brightness and reduced the contrast of the first picture (image0.png). This could emulate an overexposed image that could occur e.g. if the camera in the car has an overall dark scene, but there is a spot of sunlight on the traffic sign. The exposure of the camera would be adjusted to the overall dark frame, leading to an overexposed traffic sign.

**Performance on New Images**

All images are classified correctly by the classifier:

| filename | class index from classifier | class text | 
| --- | --- | --- |
| image_0 | 12 | Priority road |
| image_1 | 17 | No entry |
| image_2 | 3 | Speed limit (60km/h) |
| image_3 | 33 | Turn right ahead |
| image_4 | 13 | Yield |

The accuracy of this new test set is 100%. This is in good agreement to the test accuracy of the provided large test set of 97.9%. With a sample size of only five images, the expected result for the new test set is in fact 100%. There are no indications for overfitting of the model. This is visible in the non-increasing loss function of the validation set during training. This behavior was ensured by the use of regularization.

**Model Certainty - Softmax Probabilities**

The top five softmax probabilities for the classification of the newly acquired test images are as follows:

| filename | rank of class | softmax prob | class index | class text |
| --- | --- | --- | --- | --- |
| image0 | 1 |  5.87e-01 | 12 | Priority road |
| image0 | 2 |  1.78e-01 | 13 | Yield |
| image0 | 3 |  2.28e-02 | 35 | Ahead only |
| image0 | 4 |  1.91e-02 | 36 | Go straight or right |
| image0 | 5 |  1.82e-02 | 9 | No passing |
| --- | --- | --- | --- | --- |
| image1 | 1 |  1.00e+00 | 17 | No entry |
| image1 | 2 |  4.53e-14 | 14 | Stop |
| image1 | 3 |  8.17e-17 | 34 | Turn left ahead |
| image1 | 4 |  6.77e-17 | 12 | Priority road |
| image1 | 5 |  2.39e-17 | 22 | Bumpy road |
| --- | --- | --- | --- | --- |
| image2 | 1 |  1.00e+00 | 3 | Speed limit (60km/h) |
| image2 | 2 |  1.46e-15 | 35 | Ahead only |
| image2 | 3 |  1.32e-15 | 23 | Slippery road |
| image2 | 4 |  8.64e-16 | 34 | Turn left ahead |
| image2 | 5 |  6.54e-16 | 16 | Vehicles over 3.5 metric tons prohibited |
| --- | --- | --- | --- | --- |
| image3 | 1 |  1.00e+00 | 33 | Turn right ahead |
| image3 | 2 |  3.51e-14 | 37 | Go straight or left |
| image3 | 3 |  1.66e-14 | 39 | Keep left |
| image3 | 4 |  1.50e-15 | 35 | Ahead only |
| image3 | 5 |  1.25e-15 | 24 | Road narrows on the right |
| --- | --- | --- | --- | --- |
| image4 | 1 |  9.43e-01 | 17 | No entry |
| image4 | 2 |  5.72e-02 | 12 | Priority road |
| image4 | 3 |  5.91e-06 | 9 | No passing |
| image4 | 4 |  1.73e-06 | 14 | Stop |
| image4 | 5 |  1.52e-06 | 26 | Traffic signals |
| --- | --- | --- | --- | --- |

The first image, image0.png of the priority road sign, was manually modified to emulate overexposure. It is visible that the certainty of the classifier on this image is the worst over the five images at only 58.7% for the correct class. While the classification still worked, there seems to be a danger for misclassification for overexposed images. The classifier is certain on the classification of the images, image1.png to image3.png. A second highest softmax probability of 1e-14 are zero within numerical accuracy. The result for the last image, image4.png, is more interesting again. While the image is still correctly classified as a no entry sign, the probability for this class is only 94.3%. This could be due to the fact that this circular sign is mounted to the back of triangular traffic sign. This shape feature seems to stimulate some parts of the NN that would otherwise not care about no entry signs.



