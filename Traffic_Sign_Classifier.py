import numpy as np
import pickle
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import datetime
import cv2

np.random.seed(1)

trainNewModel = False
apply_Ych_centering = False
augment_by_shifting = False

##########################################################################################
##########################################################################################

# Load pickled data

training_file = 'data/train.p'
validation_file = 'data/valid.p'
testing_file = 'data/test.p'

with open(training_file, mode='rb') as f:
    train = pickle.load(f)
with open(validation_file, mode='rb') as f:
    valid = pickle.load(f)
with open(testing_file, mode='rb') as f:
    test = pickle.load(f)

X_train, y_train = train['features'], train['labels']
X_valid, y_valid = valid['features'], valid['labels']
X_test, y_test = test['features'], test['labels']


##########################################################################################
##########################################################################################


### Replace each question mark with the appropriate value.
### Use python, pandas or numpy methods rather than hard coding the results

# TODO: Number of training examples
n_train = X_train.shape[0]

# TODO: Number of validation examples
n_validation = X_valid.shape[0]

# TODO: Number of testing examples.
n_test = X_test.shape[0]

# TODO: What's the shape of an traffic sign image?
image_shape = X_train.shape[1:4]

# TODO: How many unique classes/labels there are in the dataset.
n_classes = len(np.unique(y_train))

print("Number of training examples =", n_train)
print("Number of validation examples =", n_validation)
print("Number of testing examples =", n_test)
print("Image data shape =", image_shape)
print("Number of classes =", n_classes)

def redefine_validation_set(X_train, y_train, X_valid, y_valid):
    # combine training and validation sets
    X_tmp = np.concatenate((X_train, X_valid), axis=0)
    y_tmp = np.concatenate((y_train, y_valid), axis=0)
    # select a random (but reproducible) test set from the comnined data set
    np.random.seed(1)
    indices_for_shuffling = np.random.permutation(np.arange(0, n_train+n_validation))
    X_tmp = X_tmp[indices_for_shuffling, :, :, :]
    y_tmp = y_tmp[indices_for_shuffling]
    # separate training and validation set
    X_train = X_tmp[:n_train, :, :, :]
    y_train = y_tmp[:n_train]
    X_valid = X_tmp[n_train:, :, :, :]
    y_valid = y_tmp[n_train:]

# redefine_validation_set(X_train, y_train, X_valid, y_valid)

##########################################################################################
##########################################################################################

### Data exploration visualization code goes here.
### Feel free to use as many code cells as needed.
# Visualizations will be shown in the notebook.
#%matplotlib inline
X_train_orig = np.copy(X_train)
X_test_orig = np.copy(X_test)
X_valid_orig = np.copy(X_valid)


if augment_by_shifting:
    D = 4
    X_train_up = np.copy(X_train)
    X_train_up[:, 0:32 - D, :, :, ] = X_train_up[:, D:32, :, :]
    X_train_up[:, 32 - D:32, :, :] = 255
    X_train_down = np.copy(X_train)
    X_train_down[:, D:32, :, :, ] = X_train_down[:, 0:32 - D, :, :]
    X_train_down[:, 0:D, :, :] = 255
    X_train_left = np.copy(X_train)
    X_train_left[:, :, 0:32 - D, :, ] = X_train_left[:, :, D:32, :]
    X_train_left[:, :, 32 - D:32, :] = 255
    X_train_right = np.copy(X_train)
    X_train_right[:, :, D:32, :, ] = X_train_right[:, :, 0:32 - D, :]
    X_train_right[:, :, 0:D, :] = 255
    X_train = np.concatenate((X_train, X_train_up, X_train_down, X_train_left, X_train_right), axis=0)
    y_train = np.concatenate((y_train, y_train, y_train, y_train, y_train), axis=0)

    # i_plot = 6904
    # fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(6, 6))
    # axes = axes.flatten()
    # axes[0].imshow(X_train[i_plot,:,:,:])
    # fig.delaxes(axes[1])
    # axes[2].imshow(X_train_up[i_plot, :, :, :])
    # axes[3].imshow(X_train_down[i_plot, :, :, :])
    # axes[4].imshow(X_train_left[i_plot, :, :, :])
    # axes[5].imshow(X_train_right[i_plot, :, :, :])
    # plt.show()


def plot_examples(X_train, seed):
    np.random.seed(seed)
    indices_for_plotting = np.random.randint(0, X_train.shape[0]-1, 144)
    fig, axes = plt.subplots(nrows=12, ncols=12, figsize=(12, 12))
    axes = axes.flatten()
    for i in range(144):
        axes[i].imshow(X_train[indices_for_plotting[i],:,:,:])
        axes[i].set_xticks([])
        axes[i].set_yticks([])
    plt.show()

def plot_label_occurence(y_train, y_valid, y_test):
    fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(13, 9))
    axes = axes.flatten()
    hist_train_labels = axes[0].hist(y_train, np.arange(-0.5, n_classes))
    axes[0].set_title('histo of training labels')
    hist_valid_labels = axes[1].hist(y_valid, np.arange(-0.5, n_classes))
    axes[1].set_title('histo of validation labels')
    hist_test_labels = axes[2].hist(y_test, np.arange(-0.5, n_classes))
    axes[2].set_title('histo of test labels')
    fig.delaxes(axes[3])
    axes[4].plot(np.arange(0, n_classes), np.divide(hist_valid_labels[0], hist_train_labels[0]), '.')
    axes[4].set_title('ratio of histos validation/training')
    axes[5].plot(np.arange(0, n_classes), np.divide(hist_test_labels[0], hist_train_labels[0]), '.')
    axes[5].set_title('ratio of histos test/training')
    plt.show()

# plot_examples(X_train, 1)
# plot_label_occurence(y_train, y_valid, y_test)

##########################################################################################
##########################################################################################

### Preprocess the data here. It is required to normalize the data. Other preprocessing steps could include
### converting to grayscale, etc.
### Feel free to use as many code cells as needed.

# normalize image data

def centerYchannel(X):
    for i in range(X.shape[0]):
        X_YUV = cv2.cvtColor(X[i,:,:,:], cv2.COLOR_RGB2YUV)
        X_Y = X_YUV[:,:,0]
        X_Y = X_Y.astype(int)
        X_Y = X_Y+127-int(np.mean(X_Y))
        X_Y = X_Y[:]
        X_Y[np.greater(X_Y, 255)] = 255
        X_Y[np.less(X_Y, 0)] = 0
        X_Y = X_Y.astype(np.uint8)
        X_Y = X_Y.reshape(32, 32)
        X_YUV[:,:,0] = X_Y
        X[i, :, :, :] = cv2.cvtColor(X_YUV, cv2.COLOR_YUV2RGB)

if apply_Ych_centering:
    centerYchannel(X_train)
    centerYchannel(X_test)
    centerYchannel(X_valid)



X_train = (X_train-127.5)/127.5
X_test = (X_test-127.5)/127.5
X_valid = (X_valid-127.5)/127.5

##########################################################################################
##########################################################################################

### Define your architecture here.
### Feel free to use as many code cells as needed.



def convNet(x, sigma = 0.1, bias_init = 0.01, Nof_kernels_conv1 = 32, Nof_kernels_conv2 = 64, fc1_dim = 250, fc2_dim = 120, filter_size = 3):
    # Hyperparameters
    mu = 0
    # sigma = 0.1
    # bias_init = 0.01
    #
    # Nof_kernels_conv1 = 32 # 32
    # Nof_kernels_conv2 = 64 # 64
    # fc1_dim = 250  # 250
    # fc2_dim = 120 # 120
    # filter_size = 3 # 3

    # SOLUTION: Layer 1: Convolutional. Input = 32x32x3. Output = 28x28x6.
    conv1_W = tf.Variable(tf.truncated_normal(shape=(filter_size, filter_size, 3, Nof_kernels_conv1), mean = mu, stddev = sigma))
    conv1_b = tf.Variable(bias_init*tf.ones(Nof_kernels_conv1))
    conv1   = tf.nn.conv2d(x, conv1_W, strides=[1, 1, 1, 1], padding='SAME') + conv1_b

    # SOLUTION: Activation.
    conv1 = tf.nn.relu(conv1)

    # SOLUTION: Pooling. Input = 28x28x6. Output = 14x14x6.
    conv1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    # SOLUTION: Layer 2: Convolutional. Output = 10x10x16.
    conv2_W = tf.Variable(tf.truncated_normal(shape=(filter_size, filter_size, Nof_kernels_conv1, Nof_kernels_conv2), mean = mu, stddev = sigma))
    conv2_b = tf.Variable(bias_init*tf.ones(Nof_kernels_conv2))
    conv2   = tf.nn.conv2d(conv1, conv2_W, strides=[1, 1, 1, 1], padding='SAME') + conv2_b

    # SOLUTION: Activation.
    conv2 = tf.nn.relu(conv2)

    # SOLUTION: Pooling. Input = 10x10x16. Output = 5x5x16.
    conv2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    # SOLUTION: Flatten. Input = 5x5x16. Output = 400.
    # fc0   = tf.flatten(conv2)
    fc0 = tf.reshape(conv2, [-1, 8 * 8 * Nof_kernels_conv2])
    #fc0 = tf.nn.dropout(fc0, keep_prob=keep_prob)

    # SOLUTION: Layer 3: Fully Connected. Input = 400. Output = 120.
    fc1_W = tf.Variable(tf.truncated_normal(shape=(8 * 8 * Nof_kernels_conv2, fc1_dim), mean = mu, stddev = sigma))
    fc1_b = tf.Variable(bias_init*tf.ones(fc1_dim))
    fc1   = tf.matmul(fc0, fc1_W) + fc1_b

    # SOLUTION: Activation.
    fc1    = tf.nn.relu(fc1)
    fc1 = tf.nn.dropout(fc1, keep_prob=keep_prob)

    # SOLUTION: Layer 4: Fully Connected. Input = 120. Output = 84.
    fc2_W  = tf.Variable(tf.truncated_normal(shape=(fc1_dim, fc2_dim), mean = mu, stddev = sigma))
    fc2_b  = tf.Variable(bias_init*tf.ones(fc2_dim))
    fc2    = tf.matmul(fc1, fc2_W) + fc2_b

    # SOLUTION: Activation.
    fc2    = tf.nn.relu(fc2)
    fc2 = tf.nn.dropout(fc2, keep_prob=keep_prob)

    # SOLUTION: Layer 5: Fully Connected. Input = 84. Output = 43.
    fc3_W  = tf.Variable(tf.truncated_normal(shape=(fc2_dim, 43), mean = mu, stddev = sigma))
    fc3_b  = tf.Variable(bias_init*tf.ones(43))
    logits = tf.matmul(fc2, fc3_W) + fc3_b

    L2_loss = tf.nn.l2_loss(conv1_W) + tf.nn.l2_loss(conv2_W) + tf.nn.l2_loss(fc1_W) + tf.nn.l2_loss(fc2_W) + tf.nn.l2_loss(fc3_W)

    return logits, L2_loss

def get_run_number():
    f = open('run_num.txt', 'r')
    run_num = f.read().rstrip()
    run_num = int(run_num)
    f.close()
    f = open('run_num.txt', 'w')
    f.write('{}'.format(run_num+1))
    f.close()
    return run_num
##########################################################################################
##########################################################################################

EPOCHS = 200
BATCH_SIZE = 128

x = tf.placeholder(tf.float32, (None, 32, 32, 3))
y = tf.placeholder(tf.int32, (None))
one_hot_y = tf.one_hot(y, 43)

learn_rate_ini = 0.0015 # 0.001
learn_dec_tim = 59
learn_dec_amp = 0.1

L2_loss_amplitude = 5.7E-05
DO_keep_prob = 0.5

sigma_init = 0.1
bias_init = 0.001
Nof_kernels_conv1 = 32
Nof_kernels_conv2 = 64
fc1_dim = 250 # 250
fc2_dim = 125 # 120
filter_size = 3

randomize_HP = False
if randomize_HP:
    np.random.seed(0)
    now = time.time()
    rng_seed = now - int(now)
    rng_seed = int(rng_seed * 1e6)
    np.random.seed(rng_seed)

    L2_loss_amplitude = np.float_power(10,np.random.uniform(-6, -3))
    # learn_rate_ini = np.float_power(10,np.random.uniform(-3.1, -2.5))
    # learn_dec_tim = int(np.random.uniform(40, 80))
    print('L2_loss_amp={}, learn_rate_ini={}, learn_dec_tim={}'.format(L2_loss_amplitude, learn_rate_ini, learn_dec_tim))



keep_prob = tf.placeholder(tf.float32)
logits, L2_loss = convNet(x, sigma_init, bias_init, Nof_kernels_conv1, Nof_kernels_conv2, fc1_dim, fc2_dim, filter_size)

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_y, logits=logits)

loss_operation = tf.reduce_mean(cross_entropy) + L2_loss_amplitude*L2_loss

global_step = tf.Variable(0, trainable=False)
# *=0.9 the learning rate after 4 epochs
learning_rate = tf.train.exponential_decay(learn_rate_ini, global_step, learn_dec_tim * (X_train.shape[0] / BATCH_SIZE), learn_dec_amp, staircase=True)
#training_operation = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9).minimize(loss_operation, global_step=global_step)
optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate)
training_operation = optimizer.minimize(loss_operation, global_step=global_step)

correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(one_hot_y, 1))
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
saver = tf.train.Saver()

def evaluate(X_data, y_data):
    num_examples = len(X_data)
    total_accuracy = 0
    total_loss = 0
    sess = tf.get_default_session()
    for offset in range(0, num_examples, BATCH_SIZE):
        batch_x, batch_y = X_data[offset:offset+BATCH_SIZE], y_data[offset:offset+BATCH_SIZE]
        accuracy = sess.run(accuracy_operation, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.0})
        total_accuracy += (accuracy * len(batch_x))
        total_loss += sess.run(loss_operation, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.0}) * len(batch_x)
    return total_accuracy / num_examples, total_loss / num_examples

def get_wrong_examples(X_data, y_data):
    num_examples = len(X_data)
    total_accuracy = 0
    total_loss = 0
    sess = tf.get_default_session()
    error_index_list = np.ndarray([], int)
    for offset in range(0, num_examples, BATCH_SIZE):
        batch_x, batch_y = X_data[offset:offset+BATCH_SIZE], y_data[offset:offset+BATCH_SIZE]
        batch_correctness = sess.run(correct_prediction, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.0})
        error_indices = offset+np.where(~batch_correctness)[0]
        error_index_list = np.append(error_index_list, error_indices)
    error_index_list = error_index_list[1:]
    return error_index_list


if trainNewModel:
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        num_examples = len(X_train)

        print("Training...")
        list_training_accuracy = list()
        list_validation_accuracy = list()
        list_training_loss = list()
        list_validation_loss = list()
        list_learning_rate = list()
        list_epochs = list()

        fig2, axes2 = plt.subplots(nrows=2, ncols=2, figsize=(13, 9))
        axes2 = axes2.flatten()
        plt.ion()
        # plt.show()


        for i in range(EPOCHS):
            X_train, y_train = shuffle(X_train, y_train)
            for offset in range(0, num_examples, BATCH_SIZE):
                end = offset + BATCH_SIZE
                batch_x, batch_y = X_train[offset:end], y_train[offset:end]
                sess.run(training_operation, feed_dict={x: batch_x, y: batch_y, keep_prob: DO_keep_prob})
            training_accuracy, training_loss = evaluate(X_train, y_train)
            validation_accuracy, validation_loss = evaluate(X_valid, y_valid)
            print("EPOCH {} ...".format(i + 1))
            print('Date: {}'.format(datetime.datetime.now().strftime("%y%m%d_%H%M%S")))
            print("Training Accuracy = {:.3f}".format(training_accuracy))
            print("Validation Accuracy = {:.3f}".format(validation_accuracy))
            print("training loss: {:.2e}".format(training_loss))
            print("validation loss: {:.2e}".format(validation_loss))
            print("learning rate: {:.2e}".format(sess.run(learning_rate)))
            list_epochs.append(i+1)
            list_training_accuracy.append(training_accuracy)
            list_validation_accuracy.append(validation_accuracy)
            list_training_loss.append(training_loss)
            list_validation_loss.append(validation_loss)
            list_learning_rate.append(sess.run(learning_rate))
            axes2[0].plot(list_epochs, list_training_loss, "bo-", label="training")
            axes2[0].plot(list_epochs, list_validation_loss, "ro-", label="validation")
            axes2[0].set_xlabel('epoch')
            axes2[0].set_ylabel('loss')
            axes2[0].set_yscale("log", nonposy='clip')
            axes2[0].grid(color='k')
            axes2[1].plot(list_epochs, list_training_accuracy, "bo-", label="training")
            axes2[1].plot(list_epochs, list_validation_accuracy, "ro-", label="validation")
            axes2[1].set_xlabel('epoch')
            axes2[1].set_ylabel('accuracy')
            axes2[1].grid(color='k')
            axes2[3].plot(list_epochs, list_training_accuracy, "bo-", label="training")
            axes2[3].plot(list_epochs, list_validation_accuracy, "ro-", label="validation")
            axes2[3].set_xlabel('epoch')
            axes2[3].set_ylabel('accuracy')
            axes2[3].set_ylim((0.95, 1.0))
            axes2[3].grid(color='k')
            axes2[2].plot(list_epochs, list_learning_rate, "ko-")
            axes2[2].set_xlabel('epoch')
            axes2[2].set_ylabel('learning rate')
            axes2[2].grid(color='k')
            if i == 0:
                handles0, labels0 = axes2[0].get_legend_handles_labels()
                axes2[0].legend(handles0, labels0)
                handles1, labels1 = axes2[1].get_legend_handles_labels()
                axes2[1].legend(handles1, labels1)
                handles3, labels3 = axes2[3].get_legend_handles_labels()
                axes2[3].legend(handles3, labels3)
                plt.show()
            else:
                plt.draw()
                plt.pause(0.05)
        run_num = get_run_number()
        saver.save(sess, './training_run_{}'.format(run_num))
        plt.savefig('training_run_{}.png'.format(run_num))
        print("Model saved")

        logfile = open('training.csv', 'a')
        logstring = '{};{};Adam;{};{};{};{}'.format(run_num, datetime.datetime.now().strftime("%y%m%d_%H%M%S"), learn_rate_ini, learn_dec_tim, learn_dec_amp, L2_loss_amplitude)
        logstring = '{};{};{};{};{};{};{};{};{}'.format(logstring, sigma_init, bias_init, Nof_kernels_conv1, Nof_kernels_conv2, fc1_dim, fc2_dim, filter_size, DO_keep_prob)
        logstring = '{};{};'.format(logstring,np.max(list_validation_accuracy))
        logfile.write(logstring+'\n')
        logfile.close()

else:
    # this is to read a trained model from disk
    with tf.Session() as sess:
        run_num = 71
        saver.restore(sess, './training_run_{}'.format(run_num))
        validation_error_indices = get_wrong_examples(X_valid, y_valid) # (X_valid_orig-127.5)/127.5, y_valid)
        validation_error_labels = y_valid[validation_error_indices]
        print('Number of validation errors: {}'.format(len(validation_error_indices)))
        num_training_per_class = list()
        validation_accuracy_per_class = list()
        for i in range(n_classes):
            num_training_this_class = np.sum(np.equal(y_train, i).astype(int))
            num_validation_this_class = np.sum(np.equal(y_valid, i).astype(int))
            num_validation_errors_this_class = np.sum(np.equal(validation_error_labels, i).astype(int))
            validation_accuracy_this_class = num_validation_errors_this_class / num_validation_this_class
            num_training_per_class.append(num_training_this_class)
            validation_accuracy_per_class.append(validation_accuracy_this_class)
            print('{} {} {} {}'.format(i, num_training_this_class, num_validation_errors_this_class, validation_accuracy_this_class))
        plt.plot(num_training_per_class, validation_accuracy_per_class, '.')
        plt.xlabel('number of training examples')
        plt.ylabel('validation accuracy')
        plt.title('one data point per class')
        fig, axes3 = plt.subplots(nrows=len(validation_error_indices) // 6 + 1, ncols=6, figsize=(0.5 * (len(validation_error_indices) // 6 + 1), 2.5 * 6))
        axes = axes3.flatten()
        for i in range(len(validation_error_indices)):
            axes3[i].imshow((X_valid[validation_error_indices[i], :, :, :] + 1.0) / 2.0)
            axes3[i].set_xticks([])
            axes3[i].set_yticks([])
            axes3[i].set_title('{}'.format(validation_error_indices[i]))
        for i in range(len(validation_error_indices), 6*(len(validation_error_indices)//6+1)):
            fig.delaxes(axes3[i])
        plt.show()
        plt.savefig('validation_errors_run_{}.png'.format(run_num))
